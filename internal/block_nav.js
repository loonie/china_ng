CNG.NavigationBlock = (function (_super) {
    __extends(NavigationBlock, _super);

    var CONST_ITEM_WIDTH = 100;
    var CONST_ITEM_HEIGHT = 40;
    var CONST_WIDTH_GAP = 0;
    var CONST_HEIGHT_GAP = 0;



    function NavigationBlock(host_activity, block_root_view) {
        NavigationBlock.__super__.constructor.call(this, "NavigationBlock");
        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._Timer = null;
    }

    NavigationBlock.prototype.OnKeyDown = function (ev) {

        var key_used = NavigationBlock.__super__.OnKeyDown.call(this, ev);
        if (!key_used) {
            switch (ev.keyCode) {
                case 40:
                    var block = this.GetParentPage().GetMainBlock();
                    if(block._MainView.ChildViews.length > 0 && block.data != null){
                        block.Focus();
                        var id = this.GetMetroFocusId();
                        this.RequestRedraw(id);
                    }
                    break;

            }
        }else{
            if(ev.keyCode == 37 || ev.keyCode == 39){
                this._HostActivity.ClearLoading();
                var id = this.GetMetroFocusId();
                this.GetParentPage()._SelectIndex = id;
                if(this._Timer != null)clearTimeout(this._Timer);
                var _this = this;
                // this._Timer = setTimeout(function(){
                    
                    var block = _this.GetParentPage().GetMainBlock();
                    if(_this.data[id].content.length>0){
                        block._CreateBlock(_this.data[id]);

                    }else{
                        var curr_id = id;
                        block._ClearBlock();
                        _this._HostActivity.ShowLoading(1280,720);

                        _this._HostActivity.proxy.GetTagMainData(_this.data[id].url,function(data,length){

                            _this._HostActivity.ClearLoading();
                            if(curr_id != _this.GetParentPage()._SelectIndex)return;

                            _this.data[id].content = data;

                            if(length>0){
                                length -= 1;
                            }else{
                                length = 1;
                            }
                            _this.data[id].totalPage = length;

                            block._CreateBlock(_this.data[id]);
                            //localStorage.cng_data = JSON.stringify(this.data);

                        },function(result){

                        })
                    }



                // },400)

            }
        }

        return false;
    }

    NavigationBlock.prototype.OnJavaKey = function (keycode, action) {
        switch (keycode) {
            case 4:
            default :
                return true;
        }
    }

    NavigationBlock.prototype._CreateBlock = function(data){
        this.data = data;
        this._BuildMetro();
    };

    NavigationBlock.prototype._BuildMetro = function () {
        console.log("creat navigation")


        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: 0,
            heightGap: 0
        }

        var metro_setting = {
            direction: "horizontal",
            lineMax: 1,
            visibleLength: 11,
            count: this.data.length,
            slideStyle: "whole_page_edge"
        };



        var _this = this;

        var frame_color_texture = this._TextureManager.GetColorTexture("rgba(11,111, 1, 0)");
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        );
        focus_frame.SetLineWidth(2, 0);
        var focus_setting = {
            focusFrame: focus_frame,
        }


        var callbacks = {
            onDraw: function () {
                return _this._DrawItem.apply(_this, arguments)
            },
            onClick: function () {
                return _this._Click.apply(_this, arguments)
            },
            onDrawFocus: function () {
                return _this._DrawFocusItem.apply(_this, arguments)
            },
            onDrawBluring: null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    }

    NavigationBlock.prototype._DrawItem = function (id, container_view) {

        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var width = item_info.width;
        var height = item_info.height ;

        var child_view = new Forge.LayoutView();

        var scene = this.GetParentPage();
        if(id == scene._SelectIndex && !this.IsFocus()){
            var color = "#ff3d44"
        }else{
            var color = "#aaaaaa";
        }

        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            28, undefined, "center", "top",color, new Forge.RectArea(0, 0, width, height), height
        ), this.data[id].title);
        child_view.AddView(text_view.GetLayoutView(), {x: 0, y: 0});

        container_view.AddView(child_view, new LayoutParams({
            width: width,
            height: height
        }));
    }

    NavigationBlock.prototype._Click = function (id, container_view) {

        //switch (id){
        //    case 0:
        //        var main_scene = this._HostActivity._MainScene;
        //        this._HostActivity.SceneSwitcher_.LaunchTo(main_scene);
        //        break;
        //    case 1:
        //        var playlist_scene = this._HostActivity._PlayListScene;
        //        this._HostActivity.SceneSwitcher_.LaunchTo(playlist_scene);
        //        break;
        //    case 2:
        //        var album_scene = this._HostActivity._AlbumScene;
        //        this._HostActivity.SceneSwitcher_.LaunchTo(album_scene);
        //        break;
        //    case 3:
        //        var toplist_scene = this._HostActivity._TopListScene;
        //        this._HostActivity.SceneSwitcher_.LaunchTo(toplist_scene);
        //        break;
        //    case 4 :
        //        var _this = this;
        //        var collectiont_scene = _this._HostActivity._CollectionScene;
        //        localDatabase.getAll(function(data){
        //            collectiont_scene._BuildMetroByData(data);
        //        })
        //        this._HostActivity.SceneSwitcher_.LaunchTo(collectiont_scene);
        //        break
        //}
    };

    NavigationBlock.prototype._DrawFocusItem = function (id, container_view) {

        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var width = item_info.width - CONST_WIDTH_GAP;
        var height = item_info.height - CONST_HEIGHT_GAP;

        var frame_view = new Forge.LayoutView();

        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            28, undefined, "center", "top","#ff3d44", new Forge.RectArea(0, 0, width, height), height
        ), this.data[id].title);
        frame_view.AddView(text_view.GetLayoutView(), {x: 0, y: 0});

        var focus_line_texture = this._TextureManager.GetColorTexture("rgb(255,61,68)");
        var foucs_line_view =  new Forge.LayoutView(new Forge.TextureSetting(focus_line_texture));
        frame_view.AddView(foucs_line_view, {x: 20, y: 36,width:60,height:3});

        container_view.AddView(frame_view, {x: 0, y: 0, width: width, height: height});


        return new Forge.FocusDrawingResult(frame_view, 1.0, null);
    };


    //NavigationBlock.prototype.OnFocus = function () {
    //    this.SetEnteringFocus(this.GetMetroFocusId());
    //    NavigationBlock.__super__.OnFocus.call(this);
    //}

    return NavigationBlock;

}(Forge.MetroWidget));