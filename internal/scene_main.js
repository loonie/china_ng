CNG.MainScene = (function(_super) {
    __extends(MainScene, _super);

    function MainScene(host_activity, block_root_view) {
        MainScene.__super__.constructor.call(this, "MainScene");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;

        this._SelectIndex = 0;
        
    }

    MainScene.prototype.OnJavaKey = function(keycode,action){
        if(action == 0){return true}
        switch (keycode){
            case 4:
                if(action == 1){
                    return this._HostActivity.ExitCNG();
                }
                return false;
                break;
            default :return true;
        }
    };
    MainScene.prototype._CreateScene = function(){
        setTimeout(function(){
            if(typeof jContentShellJBridge != "undefined" && typeof jContentShellJBridge.indicateHomePageLoadDone != "undefined"){
                jContentShellJBridge.indicateHomePageLoadDone();

                console.log("-----indicateHomePageLoadDone-----");
            }
            if(typeof window['noRN'] != "undefined" && window['noRN'] == true){
                if(document.getElementById("loading_bg")){
                    document.getElementById("loading_bg").style.backgroundImage="none";
                    document.getElementById("loading_bg").style.display = "none";
                }
            }
        },1000);

        this._ClearScene();
        this._BuildMetro();
    };
    MainScene.prototype._ClearScene = function(){
        if(this._MainView && this._MainView.ChildViews.length >0){
            this._MainView.ClearViews();
        }
        this.data = null;
    };

    MainScene.prototype._BuildMetro = function(data) {

        var log_texture = this._TextureManager.GetImage2("./images/dili_logo.png",false,{width:180,height:47},"RGBA_8888");
        var log_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(log_texture));
        this._MainView.AddView(log_view,{x:550,y:0,width:180,height:47});

        //navigation
        var nav_view = new Forge.LayoutView();
        this._MainView.AddView(nav_view,{x:100,y:64});
        this._NavigationBlcok = new CNG.NavigationBlock(this._HostActivity,nav_view);
        this._NavigationBlcok.SetParentPage(this);
        this._NavigationBlcok._CreateBlock(this._HostActivity.data);


        // 推荐
        var main_block_view = new Forge.LayoutView();
        this._MainView.AddView(main_block_view,{x:0,y:148});
        this._MainBlock = new CNG.MainBlock(this._HostActivity,main_block_view);
        this._MainBlock.SetParentPage(this);
        this._MainBlock._CreateBlock(this._HostActivity.data[0]);
        this._MainBlock.Focus();

  
        if(typeof jContentShellJBridge != "undefined" && typeof jContentShellJBridge.indicateHomePageLoadDone != 'undefined'){
            jContentShellJBridge.indicateHomePageLoadDone();
            console.log('indicateHomePageLoadDone');
        }


    };


    MainScene.prototype.GetMainBlock = function(){
       return this._MainBlock;
    };
    MainScene.prototype.GetNavigationBlcok = function(){
        return this._NavigationBlcok;
    };

    MainScene.prototype.OnHide = function(){
        this._MainView.SetVisibility("HIDDEN");

    };

    MainScene.prototype.OnShow= function(){
        this._MainView.SetVisibility("VISIBLE");
    };

    MainScene.prototype.GetFocus = function(){
        this.Focus();
        this._MainRecomBlock.Focus()
    };


    return MainScene;
}(Forge.SubScene));