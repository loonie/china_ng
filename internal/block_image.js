CNG.ImageBlock = (function(_super) {
    __extends(ImageBlock, _super);

    var CONST_ITEM_WIDTH = 1280;
    var CONST_ITEM_HEIGHT = 720;
    var CONST_WIDTH_GAP = 0;
    var CONST_HEIGHT_GAP = 0;
    var CONST_FOCUS_SCALE = 1.0;

    /**
     * Metro block
     *
     * @public
     * @constructor MetroWidgetSample.ImageBlock
     * @extends Forge.SubBlock
     * @param {MetroWidgetSample.MainActivity} host_activity	宿主Activity
     * @param {Forge.LayoutView} block_root_view 这个界面的Root view
     **/
    function ImageBlock(host_activity, block_root_view) {
        ImageBlock.__super__.constructor.call(this, "ImageBlock");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._ShowTextView = false;

    }

    ImageBlock.prototype.OnKeyDown = function(ev) {
        // 重载MetroWidget的OnKeyDown函数
        // 先将键值发送给MetroWidget，如果MetroWidget没处理，并且键值为方向键，说明光标移动已经到边缘
        var key_used = ImageBlock.__super__.OnKeyDown.call(this, ev);
        var id = this.GetMetroFocusId();
        if(key_used){
            switch (ev.keyCode){
                case 13:
                    if(this._ShowTextView == false){
                        this._ShowTextView = true;
                    }else{
                        this._ShowTextView = false;
                    }
                    this.RequestFocus(id);
                    this.RequestRedraw(id);

                    break;
                case 37:
                    this._ImagesTextureManage(id);
                    break;
                case 39:
                    this._ImagesTextureManage(id);

                    break;
            }
        }else{
            if(ev.keyCode == 39){
                // jQToast.alert("最后一张",1500);
                this._HostActivity.Toast("最后一张",1500);
            }
        }

        return false;
    };
    ImageBlock.prototype._CreateBlock = function(data){
        this._DataList = data;
        var length = data.length;
        this._ImagesTexture = new Array(length);
        this._SetTextureManage(length);
        this._BuildMetro();
    };

    ImageBlock.prototype._ClearBlock = function(){
        console.log("clear block");
        if(this._MainView && this._MainView.ChildViews.length > 0){
            this._MainView.ClearViews();
        }
        this._UnloadTextureManage();

        if(this.IsFocus()){
            this.OnBlur();
        }
        this._DataList = null;
    };

    ImageBlock.prototype._SetTextureManage =function(length){
        for(var i=0;i<length;i++){
            this._ImagesTexture[i]=null;
        }
        this.text_bg_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.4)");

    };

    ImageBlock.prototype._UnloadTextureManage = function(){

        if(typeof  this._ImagesTexture != "undefined" && this._ImagesTexture != null){
            for(var i=0;i<this._ImagesTexture.length;i++) {
                if (this._ImagesTexture[i] != null) {
                    this._ImagesTexture[i].UnloadTex();
                    this._ImagesTexture[i] = null;
                }
            }
        }
        this._ImagesTexture = null;

        if(typeof  this.text_bg_texture != "undefined" && this.text_bg_texture != null) {
            this.text_bg_texture.UnloadTex();
        }
        this.text_bg_texture = null;
    };

    ImageBlock.prototype._ImagesTextureManage =function(id){
        this.GetParentPage().SetArrowShow(id);
        if(id == 0 ){
            if(this._ImagesTexture[id] == null){
                this._ImagesTexture[id] = this._TextureManager.GetImage(this._DataList[id].image, true);
            }
            if(this._ImagesTexture[id+1] == null){
                this._ImagesTexture[id+1] = this._TextureManager.GetImage(this._DataList[id+1].image, true);
            }
            for(var i=id+2;i<this._DataList.length;i++){
                if(this._ImagesTexture[i] != null){
                    this._ImagesTexture[i].UnloadTex();
                    this._ImagesTexture[i] = null;
                }
            }
            console.log("_ImagesTextureManage "+id);
        }else if(id == this._DataList.length-1){
            if(this._ImagesTexture[id] == null){
                this._ImagesTexture[id] = this._TextureManager.GetImage(this._DataList[id].image, true);
            }
            if(this._ImagesTexture[id-1] == null){
                this._ImagesTexture[id-1] = this._TextureManager.GetImage(this._DataList[id-1].image, true);
            }
            for(var i =0;i<id-1;i++){
                if(this._ImagesTexture[i] != null){
                    this._ImagesTexture[i].UnloadTex();
                    this._ImagesTexture[i] = null;
                }
            }
            console.log("_ImagesTextureManage "+id);
        }else{
            if(this._ImagesTexture[id] == null){
                this._ImagesTexture[id] = this._TextureManager.GetImage(this._DataList[id].image, true);
            }
            if(this._ImagesTexture[id+1] == null){
                this._ImagesTexture[id+1] = this._TextureManager.GetImage(this._DataList[id+1].image, true);
            }
            if(this._ImagesTexture[id-1] == null){
                this._ImagesTexture[id-1] = this._TextureManager.GetImage(this._DataList[id-1].image, true);
            }
            for(var i=id+2;i<this._DataList.length;i++){
                if(this._ImagesTexture[i] != null){
                    this._ImagesTexture[i].UnloadTex();
                    this._ImagesTexture[i] = null;
                }
            }
            for(var i =0;i<id-1;i++){
                if(this._ImagesTexture[i] != null){
                    this._ImagesTexture[i].UnloadTex();
                    this._ImagesTexture[i] = null;
                }
            }
            console.log("_ImagesTextureManage "+id);
        }
    };

    ImageBlock.prototype._BuildMetro = function() {
        this._TextSet = new Forge.TextViewParams();
        this._TextSet.SetFontStyle(30, undefined, "left", "top", "#CCCCCC");
        this._TextSet.SetViewSize(new Forge.RectArea(0, 0, CONST_ITEM_WIDTH, CONST_ITEM_HEIGHT));

        this._ScaledTextSet = new Forge.TextViewParams();
        this._ScaledTextSet.SetFontStyle(parseInt(30 * CONST_FOCUS_SCALE), undefined, "left", "top", "#CCCCCC");
        this._ScaledTextSet.SetViewSize(new Forge.RectArea(0, 0, CONST_ITEM_WIDTH * CONST_FOCUS_SCALE, CONST_ITEM_HEIGHT * CONST_FOCUS_SCALE));

        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        };

        var length = this._DataList.length;
        var template = {
            "list": [
            ],
            "orientation": {
                "type": "horizontal",
                "lineMaxUnits": CONST_ITEM_HEIGHT
            }
        };
        for(var i=0; i<length; i++) {
            template.list.push( {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            });
        }
        var metro_setting = {
            template: template,
            visibleRect: new Forge.RectArea(0, 0, CONST_ITEM_WIDTH, CONST_ITEM_HEIGHT),
            slideStyle:"whole_page"
        };

        var frame_color_texture = this._TextureManager.GetColorTexture("rgba(0,0, 255, 0)");
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        );
        focus_frame.SetLineWidth(0, 0);
        var focus_setting = {
            duration:800,
            easing:null,
            focusFrame:focus_frame
        };

        var _this = this;
        var callbacks = {
            onDraw:function() { return _this._DrawItem.apply(_this, arguments); },
            onClick:function() { return _this._Click.apply(_this, arguments); },
            onDrawFocus:function() { return _this._DrawFocusItem.apply(_this, arguments) },
            onDrawBluring:null
        };
        this._ImagesTextureManage(0);
        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    };
    ImageBlock.prototype.SetImageSize = function(texture){

        var obj = {};
        obj.Height = 720;
        obj.Width =parseInt( texture.Width*(720/texture.Height));
        if(obj.Width >1280) {
            obj.Width = 1280;
            obj.Height = parseInt( texture.Height*(1280/texture.Width));
        }
        obj.x = (1280-obj.Width)/2;
        obj.y = (720 - obj.Height)/2;
        return obj;
    };

    ImageBlock.prototype._DrawItem = function(id, container_view) {
        // Caculate block color


        var item_info = this.GetItemInfo(id);
        var width = item_info.width;
        var height = item_info.height;
        // Build view

        var child_view = new Forge.LayoutView();
        if(this._DataList != null) {

            if(typeof this._HostActivity._LoadingTexture == "undefined" || this._HostActivity._LoadingTexture == null){
                this._HostActivity._LoadingTexture = this.GetIconTexture(CNG.CNG_IMAGE.LOADING_IMAGE);
            }

            var defaultView = new Forge.LayoutView(new Forge.ExternalTextureSetting(this._HostActivity._LoadingTexture));
            child_view.AddView(defaultView,{x:612,y:332,width:56,height:56});
            defaultView.StartAnimation(this._HostActivity.getRotateAnimation());

            var url = this._DataList[id].image;

            if (this._ImagesTexture[id] == null) {
                this._ImagesTexture[id] = this._TextureManager.GetImage(url, true);
            }

            var _this = this;

            var image_texture = this._ImagesTexture[id];

            var image_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(image_texture));
            if (image_texture.LoadTime != 0 && !image_texture.Unloaded) {
                console.log('getImageCallBack');
                child_view.RemoveView(defaultView);
                var image_size = this.SetImageSize(image_texture);
                child_view.AddView(image_view,{x:image_size.x,y:image_size.y,width: image_size.Width,height: image_size.Height});
                this._DrawTextView(image_view,id,image_size);
            } else {
                // child_view.AddView(image_view,{x:320,y:180,width:640,height:360});
                child_view.AddView(image_view,{x:1278,y:718,width:2,height:2});
                this.RegisterLoadImageCallback(image_texture,image_view,child_view,defaultView,id);
            }
            // var imgOnLoadCallback = function () {
            //     child_view.RemoveView(defaultView);

            //     _this._DrawImagesView(id, image_texture, child_view);
            // };
            // if (image_texture.LoadTime != 0 && !image_texture.Unloaded) {
            //     child_view.RemoveView(defaultView);
            //     _this._DrawImagesView(id, image_texture, child_view);
            // } else {
            //     image_texture.RegisterLoadImageCallback(null, imgOnLoadCallback, null);
            // }
        }
        // Add to metro
        container_view.AddView(child_view, new LayoutParams({
            width: width,
            height: height
        }));
    };

    ImageBlock.prototype._Click = function(id) {
    };

    ImageBlock.prototype._DrawFocusItem = function(id, container_view) {

        // Caculate block color
        var item_info = this.GetItemInfo(id);
        var width = item_info.width;
        var height = item_info.height;
        // Build view
        var scaled_view = new Forge.LayoutView();
        if(this._DataList != null) {

            if(typeof this._HostActivity._LoadingTexture == "undefined" || this._HostActivity._LoadingTexture == null){
                this._HostActivity._LoadingTexture = this.GetIconTexture(CNG.CNG_IMAGE.LOADING_IMAGE);
            }

            var defaultView = new Forge.LayoutView(new Forge.ExternalTextureSetting(this._HostActivity._LoadingTexture));
            scaled_view.AddView(defaultView,{x:612,y:332,width:56,height:56});
            defaultView.StartAnimation(this._HostActivity.getRotateAnimation());


            var url = this._DataList[id].image;
            if (this._ImagesTexture[id] == null) {
                this._ImagesTexture[id] = this._TextureManager.GetImage(url, true);
            }

            var _this = this;

            var image_texture = this._ImagesTexture[id];

            var image_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(image_texture));
            if (image_texture.LoadTime != 0 && !image_texture.Unloaded) {
                console.log('getImageCallBack');
                scaled_view.RemoveView(defaultView);
                var image_size = this.SetImageSize(image_texture);
                scaled_view.AddView(image_view,{x:image_size.x,y:image_size.y,width: image_size.Width,height: image_size.Height});
                this._DrawTextView(image_view,id,image_size);
            } else {
                // scaled_view.AddView(image_view,{x:320,y:180,width:640,height:360});
                scaled_view.AddView(image_view,{x:1278,y:718,width:2,height:2});
                this.RegisterLoadImageCallback(image_texture,image_view,scaled_view,defaultView,id)
            }
            // var imgOnLoadCallback = function () {
            //     scaled_view.RemoveView(defaultView);
            //     _this._DrawImagesView(id, image_texture, scaled_view);
            // };
            // if (image_texture.LoadTime != 0 && !image_texture.Unloaded) {
            //     scaled_view.RemoveView(defaultView);
            //     _this._DrawImagesView(id, image_texture, scaled_view);
            // } else {
            //     image_texture.RegisterLoadImageCallback(null, imgOnLoadCallback, null);
            // }

        }
        // Add to metro
        container_view.AddView(scaled_view, new LayoutParams({
            x:0,
            y:0,
            width: width,
            height: height
        }));
        return new Forge.FocusDrawingResult(scaled_view, CONST_FOCUS_SCALE, null);
    };
    ImageBlock.prototype._ResetImageViewParams = function(image_view, texture, id){
         var image_size = this.SetImageSize(texture);
         image_view.ResetLayoutParams(new Forge.LayoutParams({x:image_size.x,y:image_size.y,width: image_size.Width,height: image_size.Height}));
         this._DrawTextView(image_view, id, image_size);
    };
    ImageBlock.prototype.RegisterLoadImageCallback = function(image_texture, image_view, scaled_view, defaultView, id){
        var _this = this;
        var imgOnLoadCallback = function () {
            console.log('getImageCallBack');
            scaled_view.RemoveView(defaultView);
            _this._ResetImageViewParams(image_view, image_texture, id);
 
        };
       
        image_texture.RegisterLoadImageCallback(null, imgOnLoadCallback, null);
        
    };

    ImageBlock.prototype._DrawImagesView = function(id,texture,view){
        console.log(id);
        // if(typeof image_view.default_view != "undefined" && image_view.default_view != null){
        //    image_view.view.RemoveView(image_view.default_view);
        //    image_view.default_view = null;
        // }
        // if(image_view.image_view != null)return;
        console.log('_DrawImagesView');

        var image_size = this.SetImageSize(texture);

        //image
        var image_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(texture));
        view.AddView(image_view,{x:(1280-image_size.Width)/2,y:(720-image_size.Height)/2,width: image_size.Width,height: image_size.Height})
        this._DrawTextView(image_view, id, image_size);
  
    };
    ImageBlock.prototype._DrawTextView = function(image_view,id,image_size){
        if(this._ShowTextView == true) {
            //text
            // var text_clip_View  = new Forge.ClipView();
              
            var str = this._DataList[id].text + "  " + this._DataList[id].author;
            var text_height = parseInt(this._HostActivity.GetTextHeight(str, 24, image_size.Width - 40)) * 40;

            var text_bg_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(this.text_bg_texture));
            var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
                24, undefined, "left", "middle", "#ffffff", new Forge.RectArea(0, 0, image_size.Width - 40, 720), 40
            ), str);

            var real_height = text_view.GetLayoutViewRealHeight();
            if(real_height > 360){
                str = this._DataList[id].author;
                text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
                    24, undefined, "left", "middle", "#ffffff", new Forge.RectArea(0, 0, image_size.Width - 40, 40), 40
                ), str)
                real_height = 40;
            }

            text_bg_view.AddView(text_view.GetLayoutView(), {x: 20, y: 20,width:image_size.Width - 40,height:real_height});
            image_view.AddView(text_bg_view, {x: 0, y: image_size.Height - real_height - 40, width: image_size.Width, height: real_height + 40
            });
        }
    };

    return ImageBlock;
}(Forge.MetroWidget));