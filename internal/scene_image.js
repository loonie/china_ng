
    CNG.ImageScene = (function(_super) {
        __extends(ImageScene, _super);

        function ImageScene(host_activity, block_root_view) {
            ImageScene.__super__.constructor.call(this, "ImageScene");

            this._HostActivity = host_activity;
            this._TextureManager = this._HostActivity.GetTextureManager();
            this._MainView = block_root_view;

            this._SelectIndex = 0;

            this.show_textview = false;
            this._Timer = null;
            this._ImagesViews = null;
            this._KeyDowning = false;

        }

        ImageScene.prototype.OnJavaKey = function(keycode,action){
            switch (keycode){
                case 4:
                    if(action == 1){
                        this._HostActivity.SceneSwitcher_.BackToForIndex();
                        this._ClearScene();
                        return false;
                    }
                    return false;
                    break;
                default :return true;
            }
        };
        ImageScene.prototype._CreateScene = function(data){
            this._ClearScene();
            this._SetTextureManage();
            this._BuildMetro(data);
        };
        ImageScene.prototype._ClearScene = function(){
            if(this._MainView && this._MainView.ChildViews.length >0){
                this._MainView.ClearViews();
            }
            this.data = null;
            this._SelectIndex = 0;
            this._ImagesViews = null;
            this._UnloadTextureManage();
            if(typeof  this._ImageBlock != "undefined" &&this._ImageBlock != null){
                this._ImageBlock._ClearBlock();
            }
            this._ImageBlock = null;


        };
        ImageScene.prototype._SetTextureManage = function(){
            this.left_image_texture = this._TextureManager.GetImage2(CNG.CNG_IMAGE.iamge_left,true,{width:47,height:110},"RGBA_8888");
            this.right_image_texture = this._TextureManager.GetImage2(CNG.CNG_IMAGE.iamge_right,false,{width:47,height:110},"RGBA_8888");
        };
        ImageScene.prototype._UnloadTextureManage = function(){
            if(typeof this.left_image_texture != "undefined" &&  this.left_image_texture != null){
                this.left_image_texture.UnloadTex();
            }
            this.left_image_texture = null;

            if(typeof this.right_image_texture != "undefined" &&  this.right_image_texture != null){
                this.right_image_texture.UnloadTex();
            }
            this.right_image_texture = null;


        };
        ImageScene.prototype._DrawImagesView = function(id){
            if(id == 0){
                this.LoadImageById(id,this._ImagesViews[1]);
                this.LoadImageById(id+1,this._ImagesViews[2]);
            }else if(id == this.data.length -1){

            }else{
                this.LoadImageById(id-1,this._ImagesViews[0]);
                this.LoadImageById(id,this._ImagesViews[1]);
                this.LoadImageById(id+1,this._ImagesViews[2]);
            }
        };
        ImageScene.prototype.LoadImageById = function(id,image_view){
            var _this = this;
            var image_scale= {};
            var  url = this.data[id].image;
            var x = null;
            if(id == this._SelectIndex){
                x = 0;
            }else if (id < this._SelectIndex){
                x=-1280;
            }else{
                x = 1280;
            }
            image_view.id = id;
            image_view.view = new Forge.LayoutView();

            if(typeof  image_view.image_texture != "undefined" &&  image_view.image_texture != null ){
                //image_view.image_texture.UnloadTex();
                image_view.image_texture = null;
            }
            image_view.image_texture = this._TextureManager.GetImage(url,false);

            if(typeof image_view.default_view != "undefined" && image_view.default_view != null){
                image_view.view.RemoveView(image_view.default_view);
                image_view.default_view = null;
            }

            if(typeof image_view.image_view != "undefined" && image_view.image_view !=null){
                image_view.view.RemoveView(image_view.image_view);
                image_view.image_view = null
            }
            if(typeof image_view.text_bg_view != "undefined" && image_view.text_bg_view != null){
                image_view.view.RemoveView(image_view.text_bg_view);
                image_view.text_bg_view = null;
            }

            image_view.default_view = this._HostActivity.GetDefaultImageView(1000,720);
            image_view.view.AddView(image_view.default_view,{x:140,y:0,width:1000,height:720});

            this._MainView.AddView(image_view.view,{x:x,y:0,width:1280,height:720});

            var imgOnLoadCallback = function (item_layout_view) {

                if(typeof image_view.default_view != "undefined" && image_view.default_view != null){
                    image_view.view.RemoveView(image_view.default_view);
                    image_view.default_view = null;
                }
                if(image_view.image_view != null)return;


                image_scale.height = 720;
                image_scale.width =parseInt( item_layout_view.Width*(720/item_layout_view.Height));
                if(image_scale.width >1280) {
                    image_scale.width = 1280;
                    image_scale.height = parseInt( item_layout_view.Height*(1280/item_layout_view.Width));
                }



                //image
                image_view.image_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(image_view.image_texture));
                image_view.view.AddView(image_view.image_view,{x:(1280-image_scale.width)/2,y:(720-image_scale.height)/2,width: image_scale.width,height: image_scale.height})

                //text
                var str = _this.data[id].text+ "  "+_this.data[id].author;
                var text_height = parseInt(_this._HostActivity.GetTextHeight(str,24,image_scale.width-40)) *40;

                image_view.text_bg_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(_this.text_bg_texture));
                image_view.text_view =  new Forge.TextViewEx(_this._TextureManager,_this._HostActivity._setTexting(
                    24,undefined,"left","middle","#ffffff",new Forge.RectArea(0, 0, image_scale.width-40, text_height),24
                ),str);

                var real_height =  image_view.text_view.GetLayoutViewRealHeight();
                image_view.text_bg_view.AddView(image_view.text_view.GetLayoutView(),{x:20,y:20});
                image_view.image_view.AddView(image_view.text_bg_view,{x:0,y:image_scale.height-real_height-40,width:image_scale.width,height:real_height+40});

                //_this.ShowTextView(0);

            };
            image_view.image_texture.RegisterLoadImageCallback(null, imgOnLoadCallback, image_view.image_texture);


        };

        ImageScene.prototype.ShowTextView = function(show){

            if(typeof this.text_bg_view == "undefined" || this.text_bg_view == null ) return;
            if(show == null){
                this.text_bg_view.SetVisibility( this.text_bg_view.GetVisibility() == "HIDDEN"? "VISIBLE":"HIDDEN");
            }else{
                this.text_bg_view.SetVisibility( show? "VISIBLE":"HIDDEN");
            }


        };
        ImageScene.prototype.SetArrowShow = function(id){
            this.left_btn_view.SetVisibility(id == 0 ? "HIDDEN" : "VISIBLE");
            this.right_btn_view.SetVisibility(id == this.data.length - 1 ? "HIDDEN" : "VISIBLE");
        };


        ImageScene.prototype._BuildMetro = function(data) {
            this.data = data;
            this._HostActivity.ClearLoading(this._MainView);

            //left arrow
            this.left_btn_view = new LayoutView(new TextureSetting(this.left_image_texture,null,null,true));
            this._MainView.AddView(this.left_btn_view, {x:0,y:293,width:47,height:110});
            this.left_btn_view.SetZIndex(20);
            //
            ////right arrow
            this.right_btn_view = new LayoutView(new TextureSetting(this.right_image_texture,null,null,true));
            this._MainView.AddView(this.right_btn_view, {x:1233,y:293,width:47,height:110});
            this.right_btn_view.SetZIndex(20);


            var image_block_view = new Forge.LayoutView();
            this._MainView.AddView(image_block_view,{x:0,y:0});
            this._ImageBlock = new CNG.ImageBlock(this._HostActivity,image_block_view);
            this._ImageBlock.SetParentPage(this);
            this._ImageBlock._CreateBlock(data);
            this._ImageBlock.Focus();

            if(typeof  localStorage.cng_is_checked == "undefined" || localStorage.cng_is_checked == null || localStorage.cng_is_checked == "false"){
                var aim_scene = this._HostActivity._TipsScene;
                if(!this._HostActivity.is_tips_showed){
                    this._HostActivity.SceneSwitcher_.LaunchTo(this._HostActivity._TipsScene);
                }
            }




        };

        ImageScene.prototype.OnHide = function(){
            this._MainView.SetVisibility("HIDDEN");
        };

        ImageScene.prototype.OnShow= function(){
            this._MainView.SetVisibility("VISIBLE");
        };

        ImageScene.prototype.ScrollView=function(view,x,callback) {


            var animationEndCallback =  Forge.AnimationHelper.MoveViewTo(view, {x: x, y: 0}, 1000, null);

            animationEndCallback.SetAnimationListener(new AnimationListener( null,function(){
                if(callback != null){
                    callback()
                }

            }))

        };
        ImageScene.prototype.ChangeImagesViewsLocation = function(left){
            if(left){
                if(typeof this._ImagesViews[2] != "undefined" && this._ImagesViews[2] != null  ){
                    if(typeof this._ImagesViews[2].image_texture != "undefined" && this._ImagesViews[2].image_texture != null) {
                        //this._ImagesViews[2].image_texture.UnloadTex();
                    }
                    if(typeof this._ImagesViews[2].view != "undefined" && this._ImagesViews[2].view != null){

                    }
                    this._ImagesViews[2] = null;
                }
                this._ImagesViews[2] = this._ImagesViews[1];
                this._ImagesViews[1] = this._ImagesViews[0];
                this._ImagesViews[0] = {};


            }else{
                if(typeof this._ImagesViews[0] != "undefined" && this._ImagesViews[0] != null ){
                    if(typeof this._ImagesViews[0].image_texture != "undefined" && this._ImagesViews[0].image_texture != null){
                        //this._ImagesViews[0].image_texture.UnloadTex();
                    }
                    if(typeof this._ImagesViews[0].view != "undefined" && this._ImagesViews[0].view != null){
                        this._MainView.RemoveView(this._ImagesViews[0].view);
                    }

                    this._ImagesViews[0] = null;
                }
                this._ImagesViews[0] = this._ImagesViews[1];
                this._ImagesViews[1] = this._ImagesViews[2];
                this._ImagesViews[2] = {};
            }
        };


        return ImageScene;
    }(Forge.SubScene));