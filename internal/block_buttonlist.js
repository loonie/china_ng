CNG.ButtonList = (function(_super) {
    __extends(ButtonList, _super);
    var CONST_ITEM_WIDTH = 132;
    var CONST_WIDTH_GAP = 0;
    var CONST_HEIGHT_GAP = 19;
    var CONST_FOCUS_SCALE = 1;

    function ButtonList(host_activity, block_root_view) {
        ButtonList.__super__.constructor.call(this, "ButtonList");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._DrawTimesList = {};
        this._BuildMetro();
        window.ButtonList = this; // For debug
    };

    ButtonList.prototype.OnKeyDown = function(ev) {
        var key_used = ButtonList.__super__.OnKeyDown.call(this, ev);
        return false;
    };

    ButtonList.prototype._BuildMetro = function() {
        var _this = this; 
        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: 56,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        };

        var _TemplateTmp = {
            "list": [
                {
                    "blocks": {
                        "w": CONST_ITEM_WIDTH,
                        "h": 34 + CONST_HEIGHT_GAP
                    },
                    "focusable": true
                },
                {
                    "blocks": {
                        "w": CONST_ITEM_WIDTH,
                        "h": 56 + CONST_HEIGHT_GAP
                    },
                    "focusable": true
                },
            ],
            "orientation": {
                "type": "vertical",
                "lineMaxUnits": CONST_ITEM_WIDTH
            }
        };

        var metro_setting = {
            direction:"vertical",
            lineMax: 5,
            slideStyle:"seamless",
            template:_TemplateTmp,
            count: 2,
            visibleRect:new Forge.RectArea(0, 0, CONST_ITEM_WIDTH + 3, 590),
        };

        var getfocustexture = function(src){
            return _this._TextureManager.GetImage(src,true);
        };

        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            getfocustexture(CNG.CNG_IMAGE.focus_main.LeftUp),
            getfocustexture(CNG.CNG_IMAGE.focus_main.RightUp),
            getfocustexture(CNG.CNG_IMAGE.focus_main.LeftDown),
            getfocustexture(CNG.CNG_IMAGE.focus_main.RightDown),
            getfocustexture(CNG.CNG_IMAGE.focus_main.Up),
            getfocustexture(CNG.CNG_IMAGE.focus_main.Left),
            getfocustexture(CNG.CNG_IMAGE.focus_main.Right),
            getfocustexture(CNG.CNG_IMAGE.focus_main.Down)
        );

        focus_frame.SetLineWidth(0, 0);
        var focus_setting = {
            focusFrame:focus_frame,
            duration:250
        };

        var callbacks = {
            onDraw:function() { return _this._DrawItem.apply(_this, arguments); },
            onClick:function() { return _this._Click.apply(_this, arguments); },
            onDrawFocus:function() { return _this._DrawFocusItem.apply(_this, arguments);},
            onDrawBluring:null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    };

    ButtonList.prototype._Click = function(id) {
        switch(id){
            case 0:
                if(this.check_view != null){
                    if(localStorage.cng_is_checked == "true"){
                        localStorage.cng_is_checked = false;
                        this._MainView.RemoveView(this.checked_view)
                    } else {
                        localStorage.cng_is_checked = true;
                        var check_textrue = this._TextureManager.GetImage2(CNG.CNG_IMAGE.tip_checkbox_checked,false,{width:21,height:21},"RGBA_8888");
                        this.checked_view = new LayoutView(new ExternalTextureSetting(check_textrue,null,null,true));
                        this._MainView.AddView(this.checked_view, {x:7,y:7,width:21,height:21});
                    }
                };
                break;
            case 1:
                this._HostActivity.is_tips_showed = true;
                var aim_scene = this._HostActivity._ImageScene;
                this._HostActivity.SceneSwitcher_.BackTo(aim_scene);
                break

            default:break;
        }
    };
    
    ButtonList.prototype.OnBlur = function() {
        ButtonList.__super__.OnBlur.call(this);
    };

    ButtonList.prototype.OnFocus = function () {
        this.SetEnteringFocus(1);
        ButtonList.__super__.OnFocus.call(this);
    };

    ButtonList.prototype.DrawItemView = function(id, container_view,is_focus){
        var item_info = this.GetItemInfo(id);
        var width = item_info.width - CONST_WIDTH_GAP;
        var height = item_info.height - CONST_HEIGHT_GAP;
        switch(id){
            case 0:
                // tip_checkbox_normal
                if(is_focus){
                    var check_textrue = this._TextureManager.GetImage2("images/no_focus.png",false,{width:132,height:38},"RGBA_8888");
                    this.check_view = new LayoutView(new ExternalTextureSetting(check_textrue,null,null,true));
                    container_view.AddView(this.check_view, {x:0,y:-2,width:132,height:38});
                }
                
                var check_textrue = this._TextureManager.GetImage2(is_focus ? CNG.CNG_IMAGE.tip_checkbox_focus : CNG.CNG_IMAGE.tip_checkbox_normal,false,{width:21,height:21},"RGBA_8888");
                this.check_view = new LayoutView(new ExternalTextureSetting(check_textrue,null,null,true));
                container_view.AddView(this.check_view, {x:9,y:7,width:21,height:21});

                var text_viewex = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(22,undefined,"left","middle",is_focus ? "#FFFFFF":"#AAAAAA",new Forge.RectArea(0, 0, this._HostActivity.GetTextWidth("不再显示",22), height),height),"不再显示");
                container_view.AddView(text_viewex.GetLayoutView(), {x:36, y:0});
                break;
            case 1:
                var background_textrue = this._TextureManager.GetImage2(is_focus ? CNG.CNG_IMAGE.tip_btn_focus : CNG.CNG_IMAGE.tip_btn_normal,false,{width:width,height:height},"RGBA_8888");
                var background_view = new LayoutView(new ExternalTextureSetting(background_textrue,null,null,true));
                container_view.AddView(background_view, {x:0,y:0,width:width,height:height});
                break;
            default:break;
        }
    };

    ButtonList.prototype._DrawItem = function(id, container_view) {
        var item_info = this.GetItemInfo(id);
        var color_texture = this._HostActivity.COLOR_TEXTURE_CATEGARYLIST_NOR;
        var texture_setting = new Forge.ExternalTextureSetting(color_texture);

        var child_view = new Forge.LayoutView();

        this.DrawItemView(id,child_view,false);

        // Add to metro
        container_view.AddView(child_view, new LayoutParams({
            width: item_info.width - CONST_WIDTH_GAP,
            height: item_info.height - CONST_HEIGHT_GAP
        }));
        return new Forge.DrawingResult(child_view, 1.0, new Forge.Coordinate(0.5, 0.5));
    };

    ButtonList.prototype._DrawFocusItem = function(id, container_view) {
        var item_info = this.GetItemInfo(id);

        var color_texture = this._HostActivity.COLOR_TEXTURE_CATEGARYLIST_FOS;
        var texture_setting = new Forge.ExternalTextureSetting(color_texture);
        // Build view
        var scaled_view = new Forge.LayoutView();

        var scaled_width = item_info.width;// * CONST_FOCUS_SCALE;
        var scaled_height = item_info.height;// * CONST_FOCUS_SCALE;

        this.DrawItemView(id,scaled_view,true);
        // Add to metro
        container_view.AddView(scaled_view, new LayoutParams({
            width: scaled_width - CONST_WIDTH_GAP,
            height: scaled_height - CONST_HEIGHT_GAP
        }));
        return new Forge.DrawingResult(scaled_view, 1.0, new Forge.Coordinate(0.5, 0.5));
    };

    return ButtonList;
}(Forge.MetroWidget));