CNG.MainBlock = (function (_super) {
    __extends(MainBlock, _super);

    var CONST_ITEM_WIDTH = 340;
    var CONST_ITEM_HEIGHT = 274;
    var CONST_ITEM_POST_HEIGHT = 40;
    var CONST_WIDTH_GAP = 20;
    var CONST_HEIGHT_GAP = 40;


    function MainBlock(host_activity, block_root_view) {
        MainBlock.__super__.constructor.call(this, "MainBlock");
        
        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._Parser = this._HostActivity._Parser;

    }

    MainBlock.prototype.OnKeyDown = function (ev) {
        var key_used = MainBlock.__super__.OnKeyDown.call(this, ev);

        if(!key_used ){
            switch ( ev.keyCode) {
                case 38:
                   var block = this.GetParentPage().GetNavigationBlcok();
                    block.Focus();
                    var  focus_id = block.GetMetroFocusId();
                    block.RequestRedraw(focus_id);
                    break;
                case 39:
                    console.log('!key_used keyCode=39')
                    
                    if(this._CurrPage <this._TotalPage){
                        this._CurrPage ++;
                        this._HostActivity.data[this.GetParentPage()._SelectIndex].page++;
                        this._HostActivity.Toast("加载数据中",2000);
                        var url = this.GetNextPageUrl(this._Url,this._CurrPage);
                        console.log(url)
                        this.GetNextPageData(url);
                    }else{
                        var  focus_id = this.GetMetroFocusId();
                        if(focus_id != this.data.length-1 && focus_id%2 != 0){
                            this.SetEnteringFocus(this.data.length-1);
                            this.RequestFocus(this.data.length-1)
                        }else{
                            this._HostActivity.Toast("无更多数据",2000);
                        }
                    }
                    break
            }
        }


        return false;
    }

    MainBlock.prototype._CreateBlock = function(data){
        this._ClearBlock();
        this._CurrPage = data.page;
        this._TotalPage =data.totalPage;
        this._Url = data.url;

        this.data =  data.content;
        this._BuildMetro()
    };
    MainBlock.prototype._ClearBlock = function(){
        if(this._MainView && this._MainView.ChildViews.length >0){
            this._MainView.ClearViews();
        }
        this.data = null;
    }

    MainBlock.prototype._BuildMetro = function (data) {

        this.num_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)");

        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        };


        this._TemplateTmp = {
            "list": [
            //    {
            //        "blocks": {
            //            "w": 50,
            //            "h": CONST_ITEM_HEIGHT*2
            //        },
            //        "focusable": false
            //    },
            ],
            "orientation": {
                "type": "horizontal",
                "lineMaxUnits": 550
            }
        };


        for (var i = 0; i < this.data.length; i++) {

            var obj = {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };
            this._TemplateTmp.list.push(obj);
        }

        var metro_setting = {
            direction: "horizontal",
            lineMax: 1,
            visibleLength: 10,
            count: this.data.length ,
            template: this._TemplateTmp,
            visibleRect: new Forge.RectArea(0, 0, 1180, 550),
            slideStyle: "seamless",
            margin:{left:50,right:50}

        };




        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            this._HostActivity.GetIconTexture(CNG.CNG_IMAGE.FOCUS_IMAGE.LEFTUP),
            this._HostActivity.GetIconTexture(CNG.CNG_IMAGE.FOCUS_IMAGE.RIGNTUP),
            this._HostActivity.GetIconTexture(CNG.CNG_IMAGE.FOCUS_IMAGE.LEFTDOWN),
            this._HostActivity.GetIconTexture(CNG.CNG_IMAGE.FOCUS_IMAGE.RIGHTDOWN),
            this._HostActivity.GetIconTexture(CNG.CNG_IMAGE.FOCUS_IMAGE.UP),
            this._HostActivity.GetIconTexture(CNG.CNG_IMAGE.FOCUS_IMAGE.LEFT),
            this._HostActivity.GetIconTexture(CNG.CNG_IMAGE.FOCUS_IMAGE.RIGHT),
            this._HostActivity.GetIconTexture(CNG.CNG_IMAGE.FOCUS_IMAGE.DOWN)
        );

        focus_frame.SetLineWidth(30, 10);
        var focus_setting = {
            focusFrame: focus_frame,
            duration:500
        };
        var _this = this;

        var callbacks = {
            onDraw: function () {
                return _this._DrawItem.apply(_this, arguments)
            },
            onClick: function () {
                return _this._Click.apply(_this, arguments)
            },
            onDrawFocus: function () {
                return _this._DrawFocusItem.apply(_this, arguments)
            },
            onDrawBluring: null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    };

    MainBlock.prototype._DrawItem = function (id, container_view) {
        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var width = item_info.width - CONST_WIDTH_GAP;
        var height = item_info.height - CONST_HEIGHT_GAP;
        var image_height = height- CONST_ITEM_POST_HEIGHT;


        var child_view = new Forge.LayoutView();

        //default
        var default_view  = this._HostActivity.GetDefaultImageView(width,image_height);
        child_view.AddView(default_view,{x:0,y:0,width:width,height:image_height})

        //image
        var img_url = this.data[id].image;
        if(img_url!= null && img_url != "" && img_url != '""'){
            img_url = this._HostActivity.ChangeCNGImageSize(this.data[id].image,5);
        }else{
            img_url = gAssetsPath + '/images/default.jpg';
        }
        var image_texture = this._TextureManager.GetImage2(img_url,false,{width:width,height:image_height});
        var texture_setting = new Forge.ExternalTextureSetting(image_texture);
        var bg_view = new Forge.LayoutView(texture_setting);
        child_view.AddView(bg_view,{x:0,y:0,width:width,height:image_height});

        //remove default
        this._HostActivity.RegisterLoadImage(image_texture,child_view,default_view);


        var title_view = new Forge.TextViewEx(this._TextureManager,this._HostActivity._setTexting(
            24, undefined, "center", "middle", "#aaaaaa", new Forge.RectArea(0, 0, width, 35), 26
        ),this.data[id].title);

        container_view.AddView(child_view, new LayoutParams({
            width: width,
            height: width
        }));

        container_view.AddView(title_view.GetLayoutView(), {x: 0, y: image_height+14 });
    };
    MainBlock.prototype._Click = function (id, container_view) {(this._HostActivity._DetailScene)
        var url= this.data[id].url;
        var scene = this._HostActivity._ImageScene;
        scene._HostActivity.SceneSwitcher_.LaunchTo(scene);
       this.GetImageData(url,function(data){
           scene._CreateScene(data);
       });

    };

    MainBlock.prototype._DrawFocusItem = function (id, container_view) {
        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var scale_width = item_info.width - CONST_WIDTH_GAP;
        var scale_height = item_info.height - CONST_HEIGHT_GAP;
        var image_height =  scale_height - CONST_ITEM_POST_HEIGHT;

        var frame_view = new Forge.LayoutView();


        //default
        var default_view  = this._HostActivity.GetDefaultImageView(scale_width,image_height);
        frame_view.AddView(default_view,{x:0,y:0,width:scale_width,height:image_height})

        //image
        var img_url = this.data[id].image;
        if(img_url!= null && img_url != "" && img_url != '""'){
            img_url = this._HostActivity.ChangeCNGImageSize(this.data[id].image,5);
        }else{
            img_url = gAssetsPath + '/images/default.jpg';
        }
        var image_texture = this._TextureManager.GetImage2(img_url,false,{width:scale_width,height:image_height});
        var texture_setting = new Forge.ExternalTextureSetting(image_texture);
        var bg_view = new Forge.LayoutView(texture_setting);
        frame_view.AddView(bg_view,{x:0,y:0,width:scale_width,height:image_height});

        //remove default
        this._HostActivity.RegisterLoadImage(image_texture,frame_view,default_view);


        var text_set=this._HostActivity._setTexting(
            24, undefined, "center", "middle", "#ffffff", new Forge.RectArea(0, 0, scale_width, 26), 26
        )
        text_set.SetMarquee({repetition: "infinite", direction: "left", speed: "slow"});
        //title
        var title_view = new Forge.TextViewEx(this._TextureManager,text_set,this.data[id].title)

        container_view.AddView(frame_view, {
            x: 0,
            y: 0,
            width: scale_width,
            height: image_height
        });
        container_view.AddView(title_view.GetLayoutView(), {x: 0, y: image_height+14});



        return new Forge.FocusDrawingResult(frame_view, 1, null);
    };
    MainBlock.prototype.GetNextPageUrl = function(url,page){
        var url_parems = url.split("/");
        var str =url_parems[url_parems.length-1];
        var page_url = str.split(".");
        page_url[0] = page_url[0]+"/"+page;
        url_parems[url_parems.length-1] = page_url.join(".");
        url = url_parems.join("/");

        return url;
    };
    MainBlock.prototype.GetNextPageData = function(url){
        var _this = this;
        this._HostActivity.proxy.GetNextPageData(url,function(data){
            if(data.length >0){
                _this.AppendNextPageData(data);
            }else{
                // jQToast.alert("加载数据失败,请稍后再试",2000);
                _this._HostActivity.Toast("加载数据失败,请稍后再试",2000);
            }

        },function(result){
            // jQToast.alert("加载数据失败,请稍后再试",2000);
            _this._HostActivity.Toast("加载数据失败,请稍后再试",2000);

        })
    };
    MainBlock.prototype.AppendNextPageData = function(data){

        var templateTmp = {
            "list": [

            ],
            "orientation": {
                "type": "horizontal",
                "lineMaxUnits": 500
            }
        };
        for(var i=0;i<data.length;i++){
            this.data.push(data[i]);
            var obj = {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };

            templateTmp.list.push(obj);
        }
        this.GetMetroLayout().AppendTemplate(templateTmp);

    };

    MainBlock.prototype.OnFocus = function () {
        this.SetEnteringFocus(this.GetMetroFocusId());
        MainBlock.__super__.OnFocus.call(this);
    };
    MainBlock.prototype.GetImageData = function(url,callback ){

        var _this = this;
        this._HostActivity.proxy.GetImageData(url,function(images_data){


            if( images_data.length>0){
                callback(images_data)
            }else{
                // jQToast.alert("加载图片数据失败,请稍后再试!",2000);
                _this._HostActivity.Toast("加载图片数据失败,请稍后再试!",2000);
            }

        },function(){
             _this._HostActivity.Toast("加载图片数据失败,请稍后再试!",2000);
        });

            //if(typeof  localStorage.cng_is_checked == "undefined" || localStorage.cng_is_checked == null || localStorage.cng_is_checked == "false"){
            //    var aim_scene = this._HostActivity._TipsScene;
            //    if(!this._HostActivity.is_tips_showed){
            //        this._HostActivity.SceneSwitcher_.LaunchTo(this._HostActivity._TipsScene);
            //    }
            //}
    }

    return MainBlock;


}(Forge.MetroWidget));