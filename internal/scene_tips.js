CNG.TipsScene = (function(_super) {
    __extends(TipsScene, _super);

    function TipsScene(host_activity, block_root_view) {
        TipsScene.__super__.constructor.call(this, "TipsScene");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;

        this._BuildMetro();
    }

    TipsScene.prototype._BuildMetro = function() {
        var _this = this;

        var title_width = this._HostActivity.GetTextWidth("缩放模式操作提示",28)
        this._MainView.AddView(new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            28,undefined,"left","middle","#FFFFFF",new Forge.RectArea(0, 0, title_width, 28),28),"缩放模式操作提示").GetLayoutView(), {x:640 - title_width/2, y:43});


        // add tips  
        var background_textrue = this._TextureManager.GetImage(CNG.CNG_IMAGE.tip_image,false);
        var background_view = new Forge.LayoutView(new Forge.TextureSetting(background_textrue));
        this._MainView.AddView(background_view, {x:378,y:112,width:524,height:433});


        //add ContentList
        var button_view = new Forge.LayoutView();
        this._MainView.AddView(button_view, {x:578, y:572});
        this._ContentList = new CNG.ButtonList(this._HostActivity, button_view);
        this._ContentList.SetParentPage(this);
        this._ContentList.Focus();
    };

    TipsScene.prototype.GetButtonList = function(){
        return this._ButtonList;
    };

    TipsScene.prototype.OnJavaKey = function(keycode,action){
        switch (keycode){
            case 4:
                return false;
                break;
            default :return false;
        }

    };


    TipsScene.prototype.OnHide = function(){
        this._MainView.SetVisibility("HIDDEN");
    };

    TipsScene.prototype.OnShow= function(){
        this._MainView.SetVisibility("VISIBLE");
    };

    return TipsScene;
}(Forge.SubScene));