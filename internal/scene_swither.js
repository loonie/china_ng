/**
 * Created by Qcast_Dai on 2015/11/19.
 */
/**
 * Created by Qcast_Dai on 2015/7/20.
 */
// window["CNG"] = CNG || {};
CNG.SceneSwitcher = (function() {
    function SceneSwitcher() {
        this._SceneStack = [];
    }

    SceneSwitcher.prototype.LaunchTo = function(target_page) {
        if (this._SceneStack.length > 0) {
            this._SceneStack[this._SceneStack.length - 1].Hide();
        }
        this._SceneStack.push(target_page);
        target_page.Show(true);
    };

    SceneSwitcher.prototype.BackTo = function(target_page) {
        var found_page_index = -1;
        var current_page = this._SceneStack[this._SceneStack.length - 1];
        for (var i = this._SceneStack.length - 2; i >= 0; i--) {
            if (target_page === this._SceneStack[i])
                found_page_index = i;
        }
        if (found_page_index < 0) {
            Forge.ThrowError("Target PageBase not found, target page name=" + target_page.PageName);
        }

        this._SceneStack.splice(found_page_index + 1, this._SceneStack.length - found_page_index - 1);
        current_page.Hide();
        target_page.Show(true);
    };
    SceneSwitcher.prototype.BackToForIndex = function(){
        var current_page = this._SceneStack[this._SceneStack.length - 1];
        current_page.Hide();
        this._SceneStack.pop();
        current_page = this._SceneStack[this._SceneStack.length - 1];
        current_page.Show(true);

    };
    SceneSwitcher.prototype.LaunchToNoFocus = function(target_page) {
        if (this._SceneStack.length > 0) {
            this._SceneStack[this._SceneStack.length - 1].Hide();
        }
        this._SceneStack.push(target_page);
        target_page.Show(false);
    };


    SceneSwitcher.prototype.GetSceneStack = function() {
        return this._SceneStack;
    };

    return SceneSwitcher;
})();
