var __bind = function (fn, me) {
	return function () {
		return fn.apply(me, arguments);
	};
};
var __hasProp = {}.hasOwnProperty;
var __extends = function (child, parent) {
	for (var key in parent) {
		if (__hasProp.call(parent, key)) child[key] = parent[key];
	}
	function ctor() {
		this.constructor = child;
	}

	ctor.prototype = parent.prototype;
	child.prototype = new ctor();
	child.__super__ = parent.prototype;
	return child;
};

Function.prototype.property = function (prop, desc) {
	Object.defineProperty(this.prototype, prop, desc);
};

if (typeof self["__PendingDeclareClassList"] == 'undefined')
	self["__PendingDeclareClassList"] = [];

var __checkingClassRequired = function (depend_extends_class) {
	if (depend_extends_class)
		return (typeof eval(depend_extends_class) != "undefined");
};

//var __declareClass = function(declare_func, depend_extends_class) {
var __setDependsClass = function (declare_func, depend_extends_class) {
	var extends_class_loaded = __checkingClassRequired(depend_extends_class);
	if (extends_class_loaded) {
		declare_func();
		__notifyClassDefined();
	} else {
		self["__PendingDeclareClassList"].push({declare: declare_func, depends: depend_extends_class});
	}
};

var __notifyClassDefined = function () {
	var pending_class_list = self["__PendingDeclareClassList"];
	var ready_classes = [];
	for (var i = pending_class_list.length - 1; i >= 0; i--) {
		var pending_item = pending_class_list[i];
		var check_result = __checkingClassRequired(pending_item.depends);
		if (check_result) {
			pending_class_list.splice(i, 1);
			ready_classes.push(pending_item);
		}
	}
	for (var j = 0; j < ready_classes.length; j++) {
		console.log("---------------has delayed load classes-------------");
		//debugger;
		var pending_item = ready_classes[j];
		pending_item.declare();
	}
	if (ready_classes.length > 0) {
		__notifyClassDefined(); // Re-check again for new class defined
	}
};

// export namespace
if (typeof self["Forge"] == 'undefined')
	self["Forge"] = {};