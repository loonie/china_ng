﻿/**
 * Created by Qcast_Dai on 2015/11/25.
 *
 * 这是一个用js模拟Toast的方法，调用时，需要将该js文件include到<body> 标签之后。
 * 调用时候直接调用jQToast.alert("要弹出的文字",3000)第二个参数是时间(ms)，默认为3000，可省略。
 */
var jQToast = {
        alert: function (msg, time) {
            if(this.end_alert != null){
                clearTimeout(this.end_alert);
            }
            var text_container = document.getElementById("text_container");
            var toastDiv = document.getElementById("toastDiv");
            text_container.innerHTML = msg;
            //jQToast.fadeIn(toastDiv)
            this.clearfalde = true;
            //toastDiv.style.opacity = 100 / 100
            toastDiv.style.display = 'block';
            this.end_alert = setTimeout(function () {
                this.clearfalde = false;
                jQToast.fadeOut(toastDiv)
                localStorage.cnt = 0;
            }, time == null ? 3000 : time);
        },

        fadeIn: function (elem, speed, opacity) {
            //speed = speed || 20;
            //opacity = opacity || 100;
            //显示元素,并将元素值为0透明度(不可见)
            elem.style.display = 'block';

            ////iBase.SetOpacity(elem, 0);
            //elem.filters ? elem.style.filter = 'alpha(opacity=' + 100 + ')' : elem.style.opacity = 100 / 100;
            //var val = 0;
            //(function () {
            //    elem.filters ? elem.style.filter = 'alpha(opacity=' + val + ')' : elem.style.opacity = val / 100;
            //    val += 5;
            //    if (val <= opacity) {
            //        setTimeout(arguments.callee, speed)
            //    }
            //})();
        },

        fadeOut: function (elem, speed, opacity) {
            //speed = speed || 20;
            //opacity = opacity || 0;
            //var val = 100;
            //(function () {
            //    if(this.clearfalde){
            //        return;
            //    }
            //    elem.filters ? elem.style.filter = 'alpha(opacity=' + val + ')' : elem.style.opacity = val / 100;
            //    val -= 5;
            //    if (val >= opacity) {
            //        this.timer = setTimeout(arguments.callee, speed);
            //    } else if (val < 0) {
                    elem.style.display = 'none';
                //}
            //})();
        },

        init: function () {
            var toastDiv = document.createElement('div');
            var x = window.innerWidth;
            toastDiv.id = "toastDiv";
            toastDiv.style.top = '80%';
            toastDiv.style.left = 160 / 1280 * x + "px";
            toastDiv.style.padding = "0";
            toastDiv.style.width = 960 / 1280 * x + "px";
            toastDiv.style.position = "fixed";
            toastDiv.style.zIndex = "9999";
            toastDiv.style.textAlign = "center";
            toastDiv.style.display = "none";

            var msgp = document.createElement('p');
            msgp.id = "text_container";
            msgp.style.backgroundColor = "rgba(0, 0, 0, 0.8)";
            msgp.style.lineHeight = 35 / 1280 * x + "px";
            msgp.style.fontSize = 28 / 1280 * x + "px";
            msgp.style.color = "white";
            msgp.style.padding = "10px 15px";
            msgp.style.display = "inline-block";
            msgp.style.borderRadius = "5px";

            toastDiv.appendChild(msgp);
            document.body.appendChild(toastDiv);
        }
    }
    ;

//create the view container
jQToast.init();