// export namespace
if (typeof window["CNG"] == 'undefined'){
    window["CNG"] = {};
    console.log('activity cng')
}

CNG.MainActivity = (function(_super) {
    __extends(MainActivity, _super);

    /**
     * public
     * Create MainActivity
     *
     * @public
     * @constructor HelloWorld.MainActivity
     * @param {Forge.ActivityManager} manager
     **/
    function MainActivity(manager) {
        var activity_name = "CNG";
        var require_js_url = CNG.Depends;
        var version_string = CNG.JavascriptVersion;

        MainActivity.__super__.constructor.call(this, activity_name, manager, require_js_url, version_string);

        this._Manager = manager;
        this._TextureManager = this.GetTextureManager();
        this.proxy = new CNG.Proxy();
        this.is_tips_showed = false;

        this.PARENT_WIDTH = 1280;
        this.PARENT_HEIGHT = 720;
        this._BackHandler = null;
        this._BackHintView = null;
        this._Parser = new JQDomParser();

    }

    /**
     * public [Interface]
     * Called after all required script loaded for first time start
     *
     * @returns nothing
     **/
    MainActivity.prototype.OnCreate = function() {


        console.log("MainActivity OnCreate...............");
        //var _this = this;
        this._MainView = new Forge.LayoutView();

        this._PageGrandpa = new Forge.PageBase("RootDispatcher");

        this.SceneSwitcher_ = new CNG.SceneSwitcher();


        //bg_color
        var bg_color_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,1)");
        var bg_color_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(bg_color_texture));
        this._MainView.AddView(bg_color_view,{x:0,y:0,width:1280,height:720});

        //bg
        var bg_texture = this._TextureManager.GetImage2(gAssetsPath + "/images/bg.jpg",true,{width:1280,height:720},"RGBA_8888");
        var bg_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(bg_texture));
        this._MainView.AddView(bg_view,{x:0,y:0,width:1280,height:720});

        var recom_view = new Forge.LayoutView();
        this._MainView.AddView(recom_view, {x:0, y:0});
        this._MainScene = new CNG.MainScene(this, recom_view);
        this._MainScene.SetParentPage(this._PageGrandpa);
        this._MainScene.OnHide();

        var tips_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(this._TextureManager.GetColorTexture("#000000")));
        this._MainView.AddView(tips_view, {x:0, y:0,width:1280,height:720});
        this._TipsScene = new CNG.TipsScene(this, tips_view);
        this._TipsScene.SetParentPage(this._PageGrandpa);
        this._TipsScene.OnHide();


        var image_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(this._TextureManager.GetColorTexture("#000000")));
        this._MainView.AddView(image_view, {x:0, y:0,width:1280,height:720});
        this._ImageScene = new CNG.ImageScene(this, image_view);
        this._ImageScene.SetParentPage(this._PageGrandpa);
        this._ImageScene.OnHide();

        // // Focus to this block to dispatch key event

        this.SceneSwitcher_.LaunchTo(this._MainScene);

        var _this = this;
        this.ShowLoading(1280,720);
        console.log('loading data');
        this.proxy.GetRecomMainData(function(data){
            console.log("get recome data success");
            _this.ClearLoading();
            _this.data = data;
            localStorage.cng_data = JSON.stringify(_this.data);
            _this._MainScene._CreateScene();

        },function(result){
            if(typeof localStorage.cng_data != "undefined"  ){
                _this.data = JSON.parse(localStorage.cng_data);
                _this._MainScene._CreateScene();
            }
            else{
                if(typeof jContentShellJBridge != "undefined" && typeof jContentShellJBridge.indicateHomePageLoadDone != "undefined"){
                    jContentShellJBridge.indicateHomePageLoadDone();
                    console.log("-----indicateHomePageLoadDone-----");
                }
                if(typeof window['noRN'] != "undefined" && window['noRN'] == true){
                    if(document.getElementById("loading_bg")){
                        document.getElementById("loading_bg").style.backgroundImage="none";
                        document.getElementById("loading_bg").style.display = "none";
                    }
                }
                 _this.Toast("数据加载失败,请检查网络连接!",2000);
            }
        });


    };


    MainActivity.prototype.OnPreloaded = function() {
        console.log("MainActivity preloaded...............");
    };

    MainActivity.prototype.ExitCNG = function(){
        localStorage.cnt++;
        if(typeof jContentShellJBridge != "undefined" 
              && typeof jContentShellJBridge.getAppVersionString != "undefined"
              && JSON.parse(jContentShellJBridge.getAppVersionString()).version_name.split(".")[2] == 505){
            return true;
        } else {
            if (localStorage.cnt >= 2) {
                if(typeof jContentShellJBridge.backToHomepage == 'function')

                    jContentShellJBridge.backToHomepage();

                else if(typeof jContentShellJBridge.getHomePageUrl == 'function' && typeof jContentShellJBridge.startUrlInNewTab == 'function')
                {

                    var homepage_url =jContentShellJBridge.getHomePageUrl();

                    jContentShellJBridge.startUrlInNewTab(homepage_url,"{\"full_screen_mode\":false,\"disable_mouse_mode\":true}");

                }


                return false;
            }
            this.Toast("再按一次[返回]退出国家地理画廊",2000);
            return false;
        }
    }


    /**
     * public [Interface]
     * Return the main view of this activity
     * Shall be overrided
     *
     * @returns Object{View|Forge.LayoutView, Layout|Forge.LayoutParams}
     **/
    MainActivity.prototype.GetMainView = function() {
        return {View:this._MainView, Layout:new Forge.LayoutParams({x:0, y:0, width:1280, height:720})};
    };

    /**
     * (����SimpleActivity�ӿ�)����ҳ���ֵ��һ�����ڽ����������Һ;�����
     *
     * @public
     * @func OnKeyDown
     * @memberof HelloWorld.MainActivity
     * @instance
     * @param {Event} ev        ������İ����¼�
     * @return {boolean}    true: cosume the key, false: no cosume
     **/
    MainActivity.prototype.OnKeyDown = function(ev) {
        return this._PageGrandpa.DispatchKeyDown(ev);
    };





    /**
     * (����SimpleActivity�ӿ�)����JAVA�������ֵ���˳������˵�����
     *
     * @public
     * @func OnJavaKey
     * @memberof HelloWorld.MainActivity
     * @instance
     * @param {int} keycode     code of JAVA
     * @param {int} keyaction   0:down, 1:up
     * @return {boolean} true: passthrough the key, false: cosume the key
     **/
    MainActivity.prototype.OnJavaKey = function(keycode, keyaction) {
        Forge.LogM("keycode=" + keycode + " keyaction=" + keyaction);
        console.log("keycode=" + keycode + " keyaction=" + keyaction);
        var passthrough = keycode == 82 ? false : this._PageGrandpa.DispatchJavaKey(keycode, keyaction);
        console.log(passthrough );
        if (typeof passthrough == "undefined") {
            Forge.LogE("ERROR:Java key pass-through status missing");
            console.log("ERROR:Java key pass-through status missing")
            passthrough = true;
        }
        return passthrough;
    };

    MainActivity.prototype.Ajax = function(config){
        var url      = config["url"];
        var timeout  = config["timeout"];
        var duration = config["time"];
        var complete = config["complete"];
        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        xhr.open('get', url);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.timeout = duration == null ? 3000 : duration;
        xhr.ontimeout = function(){
            timeout();
        }
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    complete(xhr.responseText);
                }
            }
        }
        xhr.send();
    };

    MainActivity.prototype._setTexting = function(size,font,algin,algin_v,color,veiw_size,line_height){
        var text_set = new Forge.TextViewParams();
        text_set.SetFontStyle(size, font, algin, algin_v, color);
        text_set.SetViewSize(veiw_size);
        text_set.SetLineHeight(line_height);
        text_set.SetTextAttr(this._TextureManager.GetRenderer().TextAttr("ellipsis","break_word"))
        return text_set;
    };
    MainActivity.prototype.GetTextWidth = function(str,size){
        var text_utils = this._TextureManager.GetRenderer().GetTextUtils();
        var str_with_font = text_utils.StringWithFont(str,size);
        var txt_width = text_utils.GetTextWidth(str_with_font);

        return txt_width;
    };

    MainActivity.prototype.GetTextHeight = function(str,size,width){
        var text_utils = this._TextureManager.GetRenderer().GetTextUtils();
        var str_with_font = text_utils.StringWithFont(str,size);
        var txt_width = text_utils.GetTextWidth(str_with_font);
        return Math.ceil(txt_width/width);
    };


    MainActivity.prototype.GetIconTexture = function(image){
       return this._TextureManager.GetBase64Image(image);

    };

    MainActivity.prototype.ShowLoading = function(width,height){
        if(typeof this._LoadingTexture == "undefined" || this._LoadingTexture == null){
            this._LoadingTexture = this.GetIconTexture(CNG.CNG_IMAGE.LOADING_IMAGE);
        }
        var img_x = (width-56)/2;
        var img_y = (height-56)/2;

        this._LoadingView = new LayoutView(new Forge.ExternalTextureSetting(this._LoadingTexture));
        this._MainView.AddView(this._LoadingView,{x:img_x,y:img_y,width:56,height:56});
        this._LoadingView.StartAnimation(this.getRotateAnimation());

    };
    MainActivity.prototype.getRotateAnimation = function(){
        //if(!rotateAnimation){
        var anchor = new Forge.Vec3([0.5,0.5,1]);
        var axis = new Forge.Vec3([0,0,1]);
        var rotateAnimation = new Forge.RotateAnimation(0,-360*2000,anchor,axis,2000*2000,null,null);
        //}
        return rotateAnimation;
    };
    MainActivity.prototype.ClearLoading = function(){
        if(this._LoadingView != null){
            this._MainView.RemoveView(this._LoadingView);
            this._LoadingView = null;
        }

    };

    MainActivity.prototype.GetDefaultImageView = function(width,height){
        if(typeof this.default_view_texture == "undefined" || this.default_view_texture == null){
            this.default_view_texture = this._TextureManager.GetColorTexture("rgb(102,102,102)");
        }
        var default_view  = new Forge.LayoutView(new Forge.ExternalTextureSetting(this.default_view_texture));

        var image_width =160,image_height = 38;


        if(typeof this.default_image_texture == "undefined" || this.default_image_texture == null) {
            this.default_image_texture = this.GetIconTexture(CNG.CNG_IMAGE.DEFAULT_IMAGE);
            //this.default_image_texture = this._TextureManager.GetColorTexture("rgb(255,255,255)");
            //this.default_image_texture = this._TextureManager.GetImage2("./images/1.png",false,{width:100,height:100},"RGBA_8888");
            // this.default_image_texture = this._TextureManager.GetImage(gAssetsPath + "/images/1.png",true);
        }

        //if(typeof this.dafault_image_View == "undefined" || this.dafault_image_View == null) {
            var dafault_image_View = new Forge.LayoutView(new Forge.ExternalTextureSetting(this.default_image_texture));

        //}
        default_view.AddView(dafault_image_View,{x:(width-image_width)/2,y:(height-image_height)/2,width:image_width,height:image_height});

        return default_view;
    };

    MainActivity.prototype.RegisterLoadImage=function(image_texture,frame_view,default_view){
        var imgOnLoadCallback = function(item_layout_view) {
            if (default_view != null) {
                frame_view.RemoveView(default_view);
                default_view = null;
            }
        };
        if (image_texture.LoadTime != 0 && !image_texture.Unloaded) {
            // texture is allready loaded
            if (default_view != null) {
                frame_view.RemoveView(default_view);
                default_view = null;
            }
        } else {
            image_texture.RegisterLoadImageCallback(null, imgOnLoadCallback, null);
        }

    };

    //修改图片大小
    MainActivity.prototype.ChangeCNGImageSize = function(url,size){
        var image_parems = url.split("/");
        image_parems[3] = "rw"+size;
        url = image_parems.join("/");
        return url;
    }
    MainActivity.prototype.Toast = function(message, duartion) {

        console.log("%cToast:","color:rgb(0, 128, 255)",message);
        if (this._BackHandler != null) {
            clearTimeout(this._BackHandler);
        }

        if (this._BackHintView != null) {
            this._MainView.RemoveView(this._BackHintView);
        }
        var scale = 1;
        var scale = this.PARENT_WIDTH / 1280;
        var layout_setting = {
            "TextSize":  28 * scale,
            "TextColor": "#FFFFFF",
            "MarginTop": 580 * scale,
            "PaddingLeft": 15 * scale,
            "PaddingTop":  15 * scale,
            "BackgroundColor":"rgba(0,0,0,0.8)",
            "RoundCorner":5,
        }

        var str_width = this.GetTextWidth(message, layout_setting.TextSize) + 2;
        var texture_setting = this.GetColorSetting(layout_setting.BackgroundColor,true,layout_setting.RoundCorner);
        this._BackHintView = new Forge.LayoutView(texture_setting);
        this._MainView.AddView(this._BackHintView, {
            x: (this.PARENT_WIDTH - str_width - layout_setting.PaddingLeft * 2) / 2,
            y: layout_setting.MarginTop,
            width: str_width + layout_setting.PaddingLeft * 2,
            height: layout_setting.TextSize + layout_setting.PaddingTop * 2
        });

        var text_set = new Forge.TextViewParams();
        text_set.SetFontStyle(layout_setting.TextSize, undefined, "center", "middle", layout_setting.TextColor);
        text_set.SetViewSize(new Forge.RectArea(0, 0, str_width, layout_setting.TextSize + layout_setting.PaddingTop * 2));
        var text_view = new Forge.TextViewEx(this._TextureManager, text_set, message).GetLayoutView();
        this._BackHintView.AddView(text_view, {
            x: layout_setting.PaddingLeft,
            y: 0,
            width: str_width,
            height: layout_setting.TextSize + layout_setting.PaddingTop * 2
        });
        var _this = this;
        this._BackHandler = setTimeout(function() {
            _this._MainView.RemoveView(_this._BackHintView);
            localStorage.cnt = 0;
        },
        duartion == null ? 2000 : duartion);
    };
    MainActivity.prototype.GetColorSetting = function(color,external,corner){
        var color_texture = this._TextureManager.GetColorTexture(color);
        var mask_texture = corner != null ? new Forge.ViewRoundCornerMask(corner) : null;
        var back_setting =  external ? new Forge.ExternalTextureSetting(color_texture,mask_texture,null,true) : new Forge.TextureSetting(color_texture,mask_texture,null,true);
        return back_setting;
    };

    return MainActivity;
})(Forge.SimpleActivity);