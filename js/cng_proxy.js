CNG.Proxy = (function () {
    var req_times = 0;
    var try_times =0;
    var config = {
        url: null,
        time: 2000,
        error_callback: null,
        complete: null
    };
    var Ajax = function (config) {
        var url = config["url"];
        var complete = config["complete"];
        var error_callback = config["error_callback"];
        var headers = config["headers"];
        var timeout = config["timeout"] == null ? 2000 : config["timeout"];

        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        xhr.timeout = timeout;
        xhr.open('get', url);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        if (headers != null && headers.length > 0) {
            for (h in headers) {
                xhr.setRequestHeader(headers[h].name, headers[h].value);
            }
        }
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    req_times = 0;
                    if (complete != null) {
                        complete(xhr.responseText);
                    } else {
                        console.log(xhr.responseText);
                    }
                }
                else {
                    console.log(xhr.responseText);
                    if (req_times >= 3) {
                        req_times = 0;
                        console.warn("数据请求失败!");
                        if (error_callback != null) {
                            error_callback();
                        } else {
                            console.log(xhr.responseText);
                        }
                    } else {
                        req_times++;
                        console.info("请求超时,正在第[" + req_times + "]次尝试,Url = ", url);
                        Ajax(config);
                    }
                }
            }
        }
        xhr.send();
    };

    function _porxy() {
        this._HeadURL = "http://www.dili360.com";
        this._Parser = new JQDomParser();
    }
        // 请求传入的URL的数据
    _porxy.prototype.GatDataByUrl = function(url,callback, error_callback) {
        config.url = this._HeadURL+url;
        config.complete = callback;
        config.error_callback = error_callback;
        Ajax(config);
    };

    _porxy.prototype.GetImageData = function(url,callback, error_callback){
        var _this = this;
        config.url = this._HeadURL+url;
        config.complete = function(result){
            try{
                // var doc = document.implementation.createHTMLDocument("");
                // doc.documentElement.innerHTML = result;
                //  var doc = window.HTMLParser.parse(result);
                var doc = _this._Parser.CreateHTMLDocument(result);
                //var slider = $(result).find("#slider-one li");
                var slider =doc.querySelectorAll("#slider-one li");


                var images_data = [];
                for(var i=0;i<slider.length;i++){
                    var obj = {};
                    // obj.image = slider[i].attributes["data-source"];
                    // obj.text =  slider[i].attributes["data-text"];
                    // obj.author = slider[i].attributes["data-author"];

                    obj.image = _this._Parser.GetAttribute(slider[i], "data-source");
                    obj.text =  _this._Parser.GetAttribute(slider[i], "data-text");
                    obj.author = _this._Parser.GetAttribute(slider[i], "data-author");

                    images_data.push(obj);  
                }
                callback(images_data);
            }catch(e){
                console.log("error",e)
                if(try_times <=3 ){
                    try_times++;
                    Ajax(config);
                }else{
                    try_times = 0;
                    error_callback();
                }     
            }
        };
        config.error_callback = error_callback;
        Ajax(config);
    }

    _porxy.prototype.GetNextPageData = function(url,callback,error_callback){
        var _this = this;
        config.url = this._HeadURL+url;
        config.complete = function(result){
            try{
                // var doc = window.HTMLParser.parse(result);
                var doc = _this._Parser.CreateHTMLDocument(result);

                var kind = doc.querySelectorAll("ul.gallery-cate-list li a");

                var  list =  doc.querySelectorAll("ul.gallery-block-small li");
                var next_page_data =[];
                for(var i=0;i<list.length;i++){
                    var obj = {};  

                    // obj.url = list[i].querySelector("div.img a").attributes['href'];
                    // obj.image = list[i].querySelector("div.img img").attributes['src'];
                    // obj.title = list[i].querySelector("div.detail h3").text;
                    // obj.time = list[i].querySelector("div.detail span").text;

                    obj.url = _this._Parser.GetHref(list[i].querySelector("div.img a"));
                    obj.image = _this._Parser.GetSrc(list[i].querySelector("div.img img"));
                    obj.title = _this._Parser.GetText(list[i].querySelector("div.detail h3"));
                    if( _this.DeleteErrorData(obj.title)){
                        break;
                    }
                    obj.time = _this._Parser.GetText(list[i].querySelector("div.detail span"));

                    next_page_data.push(obj);
                }
                callback(next_page_data);
            }catch(e){
                console.log("error",e)
                if(try_times <=3 ){
                    try_times++;
                    Ajax(config);
                }else{
                    try_times = 0;
                    error_callback();
                }
            }
        };
        config.error_callback = error_callback;
        Ajax(config);
    }
    //获得new tap data
    _porxy.prototype.GetTagMainData = function(url,callback,error_callback){
        var _this = this;
        config.url = this._HeadURL+url;
        config.complete = function(result){
            try{
                var data = [];
                var doc = _this._Parser.CreateHTMLDocument(result);
                var list = doc.querySelectorAll("ul.gallery-block-small li");
                for(var i=0;i<list.length;i++){
                    var obj = {};
                    obj.url = _this._Parser.GetHref(list[i].querySelector("div.img a"));
                    obj.image = _this._Parser.GetSrc(list[i].querySelector("div.img img"));

                    obj.title = _this._Parser.GetText(list[i].querySelector("div.detail h3"));
                    obj.time = _this._Parser.GetText(list[i].querySelector("div.detail span"));
                    data.push(obj);
                }
                var length = doc.querySelectorAll("div.pagination ul li").length;
                callback(data,length);
            }catch(e){
                console.log("error",e)
                if(try_times <=3 ){
                    try_times++;
                    Ajax(config);
                }else{
                    try_times = 0;
                    error_callback();
                }
            }
        };
        config.error_callback = error_callback;
        Ajax(config);
    }

     //获得首页数据
    _porxy.prototype.GetRecomMainData = function(callback,error_callback){
        var _this = this;
        config.url = this._HeadURL+"/gallery/";
        config.complete = function(result){
            try{
                var doc = _this._Parser.CreateHTMLDocument(result);
                var kind = doc.querySelectorAll("ul.gallery-cate-list li a");

                var data = [];
                for(var i=0;i<kind.length;i++){
                
                    var obj = {};
                    // obj.title = kind[i].text;
                    obj.title = _this._Parser.GetText(kind[i]);
                    // obj.url = kind[i].attributes.href;
                    obj.url = _this._Parser.GetHref(kind[i]);
                    obj.page = 1;
                    obj.totalPage = 1;
                    obj.content = [];
                    data.push(obj);
                }
                kind = null;
                //var doc = document.implementation.createHTMLDocument("");
                //doc.documentElement.innerHTML = result;
                //var  recom_li =  $(result).find("div.right-side li ");
                var recom_li =  doc.querySelectorAll("div.right-side ul li");
                for(var i=0;i<recom_li.length;i++){
                    var obj = {};
                
                    // obj.url = recom_li[i].querySelector("div.img a").attributes.href;
                    // obj.image = recom_li[i].querySelector("div.img img").attributes.src;
                    // obj.title = recom_li[i].querySelector("div.detail h3").text;
                    // obj.time = recom_li[i].querySelector("div.detail span").text;
                    obj.url = _this._Parser.GetHref(recom_li[i].querySelector("div.img a"));
                    obj.image = _this._Parser.GetSrc(recom_li[i].querySelector("div.img img"));
                    obj.title =  _this._Parser.GetText(recom_li[i].querySelector("div.detail h3"));
                    if( _this.DeleteErrorData(obj.title)){
                        break;
                    }
                    obj.time = _this._Parser.GetText(recom_li[i].querySelector("div.detail span"));
                    data[0].content.push(obj);
                }
                recom_li= null;
                callback(data);
            }catch(e){
                console.log("error",e)
                if(try_times <=3 ){
                    try_times++;
                    Ajax(config);
                }else{
                    try_times = 0;
                    error_callback();
                }
            }
        }
        config.error_callback = error_callback;
        Ajax(config);
    }

    _porxy.prototype.DeleteErrorData = function(title){
        var error_data = ['手艺之美'];
        for(var i in error_data){
            if(title == error_data[i] ){
                return true;
            }
        }
        return false;
    }

    return _porxy;
})();