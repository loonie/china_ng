window['JQDomParser'] = (function(){
    var CONST_RN_MODEL = "RN";
    var CONST_DOCUMENT_MODEL = "DOCUMNET";
    var parser = function(){
        this.ParserModel = null;
        this.InitParserModle();
    }
    parser.prototype.InitParserModle = function(){
        if(typeof window['HTMLParser'] == 'undefined' ){
           this.ParserModel = CONST_DOCUMENT_MODEL;
        }else{
           this.ParserModel = CONST_RN_MODEL;
        }
    }

    parser.prototype.CreateHTMLDocument = function(result){
        var doc;
        switch(this.ParserModel){
            case 'RN':
                doc = window.HTMLParser.parse(result);
                break;
            case 'DOCUMNET':
                doc = document.implementation.createHTMLDocument("");
                doc.documentElement.innerHTML = result;
                break;
        }
        return doc;
    };
    parser.prototype.GetAttribute = function(element, name){
        var attribute;
        switch(this.ParserModel){
            case 'RN':
                attribute = element.attributes[name];
                break;
            case 'DOCUMNET':
                attribute = element.getAttribute(name);
                break;
        }
        return attribute;
    };
    parser.prototype.GetText = function(element){
        var text;
        switch(this.ParserModel){
            case 'RN':
                text = element.text;
                break;
            case 'DOCUMNET':
                text = element.innerHTML;
                break;
        }
        return text;
    }

    parser.prototype.GetHref = function(element) {
        return this.GetAttribute(element,"href");
    };
    parser.prototype.GetSrc = function(element) {
        return this.GetAttribute(element,"src");
    };

    return parser;
})();