function Main()
{
    //设置浏览器，使允许跨域
    if (typeof document.grantAccessRight != "undefined"){
        document.grantAccessRight();
        document.assignNewUserAgent("Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36");

    }


    // Update canvas size
    var initcanvas = function(){
        var canvas=document.getElementById("RenderingSurface");
        canvas.OuterWidth=window.innerWidth;
        canvas.OuterHeight=window.innerHeight;
    }


    initcanvas();

    // Init Activity Manager
    gActivityManager = new Forge.ActivityManager(document.getElementById("RenderingSurface"));

    // Register activities
    var metro_activity = new CNG.MainActivity(gActivityManager);
         // Home page activity

    gActivityManager.RegisterActivity(metro_activity)
    // Do preload
    // metro_activity.PreloadRequireJs();

    // Start home page
    setTimeout(function() {
        gActivityManager.StartActivity(new Forge.Intent("CNG"));
    }, 200)
}

var gActivityManager = null;

function dispatchNetworkTypeStatus(msg){
    console.log(msg)
}

Array.prototype.remove=function(dx) {
    if(isNaN(dx)||dx>this.length){return false;}
    for(var i=0,n=0;i<this.length;i++) {
        if(this[i]!=this[dx]){
            this[n++]=this[i]
        }
    }
    this.length-=1
};



localStorage.cnt = 0;
window['gAssetsPath'] = ".";
window['noRN'] = true;


document.addEventListener("DOMContentLoaded", function(){
    var bg_imag = document.querySelector("#loading_bg");
    if(bg_imag){
        bg_imag.width = window.innerWidth;
        bg_imag.height = window.innerHeight;
    }
    Main();
    return;

});